/**
 * Created by JEVENISSE on 13/12/2016.
 */

"use strict";

let Quiz = require('./models/quizModel');

let user = require('./services/userService');
let quiz = require('./services/quizService');
let quiz_result = require('./services/quiz_resultService');
let question = require('./services/questionService');
let category = require('./services/categoryService');

let express = require('express');

let apiRouter = express.Router();

let User = require("./models/userModel");

exports.myRouter = function (app) {

    //user
    apiRouter.post('/users/login', user.login);
    apiRouter.post('/users/logout', user.logout);
    apiRouter.get('/users/relog', user.relog);
    apiRouter.get('/users', (req, res, next) => {
        auth(req, res, next, [2, 3])
    }, user.getUsers);
    apiRouter.get('/users/:user_id', (req, res, next) => {
        if(req.params.user_id === "0")
            auth(req, res, next, [1, 2, 3]);
        else
            auth(req, res, next, [2, 3])
    }, myRoutes, user.getUser);
    apiRouter.put('/users', (req, res, next) => {
        auth(req, res, next, [3])
    }, user.inputValidation, user.createUser);
    apiRouter.post('/users/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, user.updateUser);
    apiRouter.delete('/users/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, user.deleteUser);

    //quiz
    apiRouter.get('/quiz', (req, res, next) => {
        auth(req, res, next, [3])
    }, quiz.getQuizs);
    apiRouter.get('/quiz/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, function (req, res) {
        quiz.getQuiz(req, req.params.id)
            .then(item => {
                res.send(new Quiz(item));
            })
    });
    apiRouter.put('/quiz', (req, res, next) => {
        auth(req, res, next, [3])
    }, quiz.createQuiz);
    apiRouter.post('/quiz/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, quiz.updateQuiz);
    apiRouter.delete('/quiz/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, quiz.deleteQuiz);
    apiRouter.put('/quiz/:id/:question_id', (req, res, next) => {
        auth(req, res, next, [3])
    }, quiz.addQuestion);
    apiRouter.delete('/quiz/:id/:question_id', (req, res, next) => {
        auth(req, res, next, [3])
    }, quiz.deleteQuestion);

    //quiz_result
    apiRouter.put('/quiz-results/:user_id/:quiz_id', (req, res, next) => {
        auth(req, res, next, [3])
    }, quiz_result.createQuizResult);
    apiRouter.delete('/quiz-results/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, quiz_result.deleteQuizResult);
    apiRouter.get('/quiz-results/:user_id', (req, res, next) => {
        if(req.params.user_id === "0")
            auth(req, res, next, [1, 2, 3]);
        else
            auth(req, res, next, [2, 3])
    }, myRoutes, quiz_result.getQuizResultByUser);
    apiRouter.post('/quiz-results/:id', (req, res, next) => {
        auth(req, res, next, [1, 2])
    }, quiz_result.updateQuizResult);
    apiRouter.post('/quiz-results/finish/:id', (req, res, next) => {
        auth(req, res, next, [1, 2])
    }, quiz_result.finishQuizResult);

    //question
    apiRouter.put('/questions', (req, res, next) => {
        auth(req, res, next, [3])
    }, question.createQuestion);
    apiRouter.get('/questions', (req, res, next) => {
        auth(req, res, next, [3])
    }, question.getQuestions);
    apiRouter.get('/questions/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, question.getQuestion);
    apiRouter.post('/questions/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, question.updateQuestion);
    apiRouter.delete('/questions/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, question.deleteQuestion);

    //category
    apiRouter.put('/categories', (req, res, next) => {
        auth(req, res, next, [3])
    }, category.createCategory);
    apiRouter.get('/categories', (req, res, next) => {
        auth(req, res, next, [3])
    }, category.getCategories);
    apiRouter.post('/categories/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, category.updateCategory);
    apiRouter.delete('/categories/:id', (req, res, next) => {
        auth(req, res, next, [3])
    }, category.deleteCategory);

    apiRouter.get('/params', function (req, res) {
        let quizStatus = {};
        let userRoles = {};

        quizStatus[0] = "À faire";
        quizStatus[1] = "En cours";
        quizStatus[2] = "Terminé";
        quizStatus[3] = "Corrigé";

        userRoles[1] = "Candidat";
        userRoles[2] = "Correcteur";
        userRoles[3] = "Administrateur";

        res.send({quizStatus, userRoles});
    });


    app.use('/api', apiRouter);

};

let auth = function (req, res, next, role) {
    function sendNotLogged() {
        res.status(403);
        res.send("Not logged In");
    }

    if (!req.session.user) {
        sendNotLogged();
        return;
    }

    req.db.models.user.one({"email": req.session.user.email},
        function (err, user) {
            if (!user || role.indexOf(user.role) === -1) {
                sendNotLogged();
                return;
            }
            req.session.user = user;
            next();
        });
};

let myRoutes = function (req, res, next) {
    if (req.params.user_id === "0")
        req.params.user_id = req.session.user.id;

    next();
};
