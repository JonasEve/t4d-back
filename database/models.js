/**
 * Created by JEVENISSE on 13/12/2016.
 */
checkError = function(cb, err)
{
    if (err)
        return cb(err);
    return cb();
};

module.exports = function(db, cb) {

    //db.settings.set("properties.association_key", "{name}");

    db.load("./user.js", function (err) {
        checkError(cb, err)
    });
    db.load("./category.js", function (err) {
        checkError(cb, err)
    });
    db.load("./question.js", function (err) {
        checkError(cb, err)
    });
    db.load("./answer.js", function (err) {
        checkError(cb, err)
    });
    db.load("./quiz.js", function (err) {
        checkError(cb, err)
    });
    db.load("./quiz_result.js", function (err) {
        checkError(cb, err)
    });

    //db.sync();
};