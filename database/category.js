/**
 * Created by JEVENISSE on 11/01/2017.
 */

"use strict";

module.exports = function (db) {

    let Category = db.define('category', {
        label: String,
    });

    Category.hasOne('parent', Category, {reverse: "children"});
};