/**
 * Created by JEVENISSE on 11/01/2017.
 */

"use strict";

module.exports = function (db) {

    let Category = db.models.category;

    let Question = db.define('question', {
        label: String,
        type: Number,
    }, {
        autoFetch : true
    });

    Question.hasMany('category', Category, {}, {key: true, reverse: "questions"});
};