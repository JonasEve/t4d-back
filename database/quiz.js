/**
 * Created by JEVENISSE on 11/01/2017.
 */

"use strict";

module.exports = function (db) {

    let Question = db.models.question;

    let Quiz = db.define('quiz', {
            name: String,
        });

    Quiz.hasMany('question', Question, {
        position: Number,
        weight: Number
    }, {key: true, autoFetch: true});
};