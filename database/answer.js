/**
 * Created by JEVENISSE on 11/01/2017.
 */

"use strict";

module.exports = function (db) {
    let Question = db.models.question;

    let Answer = db.define('answer', {
        label: String,
        correct: Boolean
    });

    Answer.hasOne('question', Question, {reverse: 'answers', autoFetch: true});
};