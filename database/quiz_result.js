/**
 * Created by JEVENISSE on 11/01/2017.
 */

"use strict";

module.exports = function (db) {

    let User = db.models.user;

    let Quiz_result = db.define('quiz_result', {
        data: String,
        date_creation: Date,
        status: Number
    });

    Quiz_result.hasOne('user', User, {required: true, reverse: 'results'});
};