/**
 * Created by JEVENISSE on 13/12/2016.
 */

module.exports = function (db) {
    db.define('user', {
            firstname: String,
            surname: String,
            email: String,
            password: String,
            role: Number,
        });
};