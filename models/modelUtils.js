"use strict";

exports.convertArray = function (list, clazz, args) {

    let listModel = [];

    if (list == null)
        return listModel;

    for (let i = 0; i < list.length; i++) {
        listModel.push(new clazz(list[i], args));
    }

    return listModel;
};
