/**
 * Created by JEVENISSE on 20/01/2017.
 */

"use strict";

module.exports = class {
    constructor(answer, displayCorrect) {
        this.id = answer.id;
        this.label = answer.label;

        if(displayCorrect)
            this.correct = answer.correct;

        if(answer.userValue || answer.userValue === false)
            this.userValue = answer.userValue;
    }
};