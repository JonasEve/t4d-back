/**
 * Created by JEVENISSE on 20/01/2017.
 */

"use strict";

let Category = require("../models/categoryModel");
let Answer = require("../models/answerModel");

let modelUtils = require("./modelUtils");

module.exports = class{
    constructor(question, displayCorrectAnswer) {
        this.id = question.id;
        this.label = question.label;
        this.type = question.type;
        this.categories = modelUtils.convertArray(question.category, Category);
        this.answers = modelUtils.convertArray(question.answers, Answer, displayCorrectAnswer);
        if(question.extra){
            this.position = question.extra.position;
            this.weight = question.extra.weight;
        }

        if(question.correct || question.correct === false){
            this.correct = question.correct;
        }
    }
};
