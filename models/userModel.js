"use strict";

let Quiz = require("../models/quizModel");
let modelUtils = require("./modelUtils");

module.exports = class {
    constructor(user, isSession) {

        if(!isSession) {

            this.id = user.id;

            if(user.results) {
                let quizsToConvert = [];

                user.results.forEach(item => {
                    let quiz = JSON.parse(item.data);
                    quiz.status = item.status;
                    quiz.dateCreation = item.date_creation;
                    quiz.id = item.id;

                    quizsToConvert.push(quiz);
                });

                this.quizs = modelUtils.convertArray(quizsToConvert, Quiz, true);
            }
        }

        this.firstname = user.firstname;
        this.surname = user.surname;
        this.fullname = user.firstname + " " +  user.surname;
        this.role = user.role;
        this.email = user.email;
    }
};
