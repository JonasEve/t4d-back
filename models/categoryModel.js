/**
 * Created by JEVENISSE on 20/01/2017.
 */

"use strict";

let modelUtils = require("./modelUtils");

let Category = class{
    constructor(category) {
        this.id = category.id;
        this.label = category.label;
        this.children = modelUtils.convertArray(category.children, Category);
    }
};

module.exports = Category;