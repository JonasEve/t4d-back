/**
 * Created by JEVENISSE on 23/01/2017.
 */

"use strict";

let Question = require("../models/questionModel");
let modelUtils = require("./modelUtils");

module.exports = class{
    constructor(quiz) {
        this.id = quiz.id;
        this.name = quiz.name;

        let displayCorrectAnswer = (quiz.status && quiz.status === 3);
        this.questions = modelUtils.convertArray(quiz.question, Question, displayCorrectAnswer);

        if(quiz.status || quiz.status === 0)
            this.status = quiz.status;

        if(quiz.dateCreation)
            this.dateCreation = quiz.dateCreation;

        if((quiz.score || quiz.score === 0) && displayCorrectAnswer)
            this.score = quiz.score;
    }
};