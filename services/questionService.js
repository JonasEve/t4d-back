/**
 * Created by JEVENISSE on 16/01/2017.
 */

"use strict";

let base = require('./baseService');

let questionRepository = require('../repository/questionRepository');
let answerRepository = require('../repository/answerRepository');

let modelutils = require('../models/modelUtils');
let Question = require('../models/questionModel');

exports.createQuestion = function (req, res) {
    questionRepository.createQuestion(req.db.models.question, req.body)
        .then(item => {
            res.send(new Question(item));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.getQuestions = function (req, res) {

    questionRepository.getQuestions(req.db.models.question)
        .then(items => {
            res.send(modelutils.convertArray(items, Question, true));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.getQuestion = function (req, res) {

    questionRepository.getQuestion(req.db.models.question, req.params.id)
        .then(item => {
            res.send(new Question(item, true));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.updateQuestion = function (req, res) {

    let promises = [];

    questionRepository.updateQuestion(req.db.models.question, req.params.id, req.body)
        .then(updatedQuestion => {

            answerRepository.getAnswersByQuestionId(req.db.models.answer, updatedQuestion.id)
                .then(oldAnswers => {

                    let find;

                    oldAnswers.forEach(function (oldAnswer) {

                        find = false;

                        updatedQuestion.answers.every(function (newAnswer) {
                            if (oldAnswer.id === newAnswer.id) {
                                promises.push(answerRepository.updateAnswer(req.db.models.answer, oldAnswer.id, newAnswer)
                                    .catch(err => {
                                        base.handleError(res, err);
                                    }));
                                find = true;
                            }
                            return !find;
                        });

                        if (!find) {
                            promises.push(answerRepository.deteleteAnswer(req.db.models.answer, oldAnswer.id)
                                .catch(err => {
                                    base.handleError(res, err);
                                }));
                        }
                    });

                    Promise.all(promises).then(() => {
                        res.send(new Question(updatedQuestion));
                    });

                }).catch(err => {
                base.handleError(res, err);
            });

        }).catch(err => {
        base.handleError(res, err);
    });
};

exports.deleteQuestion = function (req, res) {

    questionRepository.deleteQuestion(req.db.models.question, req.params.id)
        .then(() => {
            res.sendStatus(200);
        })
        .catch(err => {
            base.handleError(res, err);
        });
};