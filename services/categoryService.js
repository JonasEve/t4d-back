/**
 * Created by JEVENISSE on 16/01/2017.
 */

"use strict";

let base = require('./baseService');
let categoryRepository = require('../repository/categoryRepository');

let modelutils = require('../models/modelUtils');
let Category = require('../models/categoryModel');

exports.createCategory = function(req, res) {

    categoryRepository.createCategory(req.db.models.category, req.body)
        .then(item => {
            res.send(new Category(item));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.getCategories = function (req, res) {

    categoryRepository.getCategories(req.db.models.category)
        .then(items => {
            res.send(modelutils.convertArray(items, Category));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.updateCategory = function(req, res) {

    categoryRepository.updateCategory(req.db.models.category, req.params.id, req.body)
        .then(item => {
            res.send(new Category(item));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.deleteCategory = function(req, res) {

    categoryRepository.deleteCategory(req.db.models.category, req.params.id)
        .then(() => {
            res.sendStatus(200);
        })
        .catch(err => {
            base.handleError(res, err);
        });
};