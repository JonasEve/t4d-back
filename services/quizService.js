/**
 * Created by JEVENISSE on 11/01/2017.
 */

"use strict";

let base = require('./baseService');
let quizRepository = require('../repository/quizRepository');
let questionRepository = require('../repository/questionRepository');

let modelutils = require('../models/modelUtils');
let Quiz = require('../models/quizModel');

exports.getQuizs = function (req, res) {

    quizRepository.getQuizs(req.db.models.quiz)
        .then(items => {

            let promises = [];

            items.forEach(quiz => {
                quiz.question.forEach(question => {
                    promises.push(questionRepository.getQuestion(req.db.models.question, question.id)
                        .then(item => {
                            question.answers = item.answers;
                            question.category = item.category;
                        })
                        .catch(err => {
                            base.handleError(res, err);
                        }));
                })
            });

            Promise.all(promises).then(() => {
                res.send(modelutils.convertArray(items, Quiz));
            });
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.getQuiz = function (req, id) {

    return new Promise(resolve => {
        quizRepository.getQuiz(req.db.models.quiz, id)
            .then(quiz => {

                let promises = [];

                quiz.question.forEach(question => {
                    promises.push(questionRepository.getQuestion(req.db.models.question, question.id)
                        .then(item => {
                            question.answers = item.answers;
                            question.category = item.category;
                        })
                        .catch(err => {
                            base.handleError(res, err);
                        }));
                });

                Promise.all(promises).then(() => {
                    resolve(quiz);
                });
            })
            .catch(err => {
                base.handleError(res, err);
            });
    })
};

exports.createQuiz = function (req, res) {
    quizRepository.createQuiz(req.db.models.quiz, req.body)
        .then(item => {
            res.send(new Quiz(item));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};


exports.updateQuiz = function (req, res) {
    quizRepository.updateQuiz(req.db.models.quiz, req.params.id, req.body)
        .then(item => {
            res.send(new Quiz(item));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.deleteQuiz = function (req, res) {
    quizRepository.deleteQuiz(req.db.models.quiz, req.params.id)
        .then(() => {
            res.send(null);
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.addQuestion = function (req, res) {

    questionRepository.getQuestion(req.db.models.question, req.params.question_id)
        .then(item => {
                if (!item) {
                    res.sendStatus(500);
                } else {
                    quizRepository.addQuestion(req.db.models.quiz, req.params.id, item)
                        .then(() => {
                            res.send(null);
                        })
                        .catch(err => {
                            base.handleError(res, err);
                        })
                }
            }
        )
        .catch(err => {
            base.handleError(res, err);
        })
};

exports.deleteQuestion = function (req, res) {

    questionRepository.getQuestion(req.db.models.question, req.params.question_id)
        .then(item => {
                if (!item) {
                    res.sendStatus(500);
                } else {
                    quizRepository.deleteQuestion(req.db.models.quiz, req.params.id, item)
                        .then(() => {
                            res.send(null);
                        })
                        .catch(err => {
                            base.handleError(res, err);
                        })
                }
            }
        )
        .catch(err => {
            base.handleError(res, err);
        });
};