/**
 * Created by JEVENISSE on 16/01/2017.
 */

"use strict";

let base = require('./baseService');
let quizResultRepository = require('../repository/quiz_resultRepository');
let questionRepository = require('../repository/questionRepository');
let quizService = require('../services/quizService');

let modelutils = require('../models/modelUtils');
let Quiz = require('../models/quizModel');

exports.createQuizResult = function (req, res) {

    quizService.getQuiz(req, req.params.quiz_id)
        .then(quiz => {
            quizResultRepository.createQuizResult(req.db.models.quiz_result, req.params.user_id, quiz)
                .then(() => {
                    res.send(null);
                })
                .catch(err => {
                    base.handleError(res, err);
                });
        });
};

exports.deleteQuizResult = function (req, res) {

    quizResultRepository.deleteQuizResult(req.db.models.quiz_result, req.params.id)
        .then(() => {
            res.send(null);
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.getQuizResultByUser = function (req, res) {

    quizResultRepository.getQuizResultByUser(req.db.models.quiz_result, req.params.user_id)
        .then(items => {

            let quizsToConvert = [];

            items.forEach(item => {
                let quiz = JSON.parse(item.data);
                quiz.status = item.status;
                quiz.dateCreation = item.date_creation;
                quiz.id = item.id;

                quizsToConvert.push(quiz);
            });

            res.send(modelutils.convertArray(quizsToConvert, Quiz));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.updateQuizResult = function (req, res) {

    req.body.status = 1;

    quizResultRepository.updateQuizResult(req.db.models.quiz_result, req.params.id, req.body)
        .then(item => {

            let quiz = JSON.parse(item.data);

            quiz.status = item.status;
            quiz.dateCreation = item.date_creation;
            quiz.id = item.id;

            res.send(new Quiz(quiz));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.finishQuizResult = function (req, res) {

    let result = req.body;
    result.score = 0;

    new Promise((resolve) => {

        let allIsCorrected = true;
        let promises = [];

        result.questions.forEach(question => {
            promises.push(new Promise((resolve, reject) => {
                questionRepository.getQuestion(req.db.models.question, question.id)
                    .then(item => {

                        let correct = true;

                        switch (question.type) {
                            case 0 :
                                for (let i = 0; i < item.answers.length; i++) {
                                    correct = (item.answers[i].correct === !!question.answers[i].userValue);
                                    if (!correct)
                                        break;
                                }
                                break;

                            default:
                                allIsCorrected = false;
                                correct = false;
                                break;
                        }

                        question.correct = correct;

                        if (correct)
                            result.score++;

                        resolve();
                    })
                    .catch(err => {
                            base.handleError(res, err);
                        }
                    )

            }));
        });

        Promise.all(promises)
            .then(() => {
                result.status = allIsCorrected ? 3 : 2;
                resolve();
            })
    })
        .then(() => {
            quizResultRepository.updateQuizResult(req.db.models.quiz_result, req.params.id, result)
                .then(item => {

                    let quiz = JSON.parse(item.data);

                    quiz.status = item.status;
                    quiz.dateCreation = item.date_creation;
                    quiz.id = item.id;

                    res.send(new Quiz(quiz));
                })
                .catch(err => {
                    base.handleError(res, err);
                });
        })
        .catch(err => {
            base.handleError(res, err);
        })
};