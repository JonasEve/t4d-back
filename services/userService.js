"use strict";

let User = require("../models/userModel");
let base = require('./baseService');
let userRepository = require('../repository/userRepository');
let quizService = require('../services/quizService');
let quizResultRepository = require('../repository/quiz_resultRepository');
let modelutils = require('../models/modelUtils');


//region Services
exports.createUser = function (req, res) {

    userRepository.createUser(req.db.models.user, req.body)
        .then(item => {
            res.send(new User(item));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.getUsers = function (req, res) {

    userRepository.getUsers(req.db.models.user)
        .then(items => {
            res.send(modelutils.convertArray(items, User));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.getUser = function (req, res) {

    userRepository.getUser(req.db.models.user, req.params.user_id)
        .then(item => {
            res.send(new User(item));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.deleteUser = function (req, res) {

    quizResultRepository.deleteQuizResultByUserId(req.db.models.quiz_result, req.params.id)
        .then(() => {
            userRepository.deleteUser(req.db.models.user, req.params.id)
                .then(() => {
                    res.send(null);
                })
                .catch(err => {
                    base.handleError(res, err);
                });
        })
        .catch(err => {
            base.handleError(res, err);
        })
};

exports.updateUser = function (req, res) {

    userRepository.updateUser(req.db.models.user, req.params.id, req.body)
        .then(item => {
            res.send(new User(item));
        })
        .catch(err => {
            base.handleError(res, err);
        });
};

exports.login = function (req, res) {

    userRepository.findUser(req.db.models.user, req.body.email)
        .then(item => {

            if (req.body.password !== item.password) {
                base.sendError(res, {status: 403});
            } else {
                req.session.user = item;
                res.send(new User(item, true));
            }
        })
        .catch(err => {
            base.handleError(res, err);
        })

};

exports.relog = function (req, res) {

    if (!req.session.user)
        res.send(null);
    else {
        res.send(new User(req.session.user, true));
    }
};

exports.logout = function (req, res) {
    req.session.destroy();
    res.send(null);
};

//endregion

//region middleware

exports.inputValidation = function (req, res, next) {

    if (!req.body.email || !req.body.firstname || !req.body.surname || !req.body.password) {
        base.sendError(res, {status: 400, message: "Missing mandatory field"});
        return;
    }

    if (!req.body.role)
        req.body.role = 1;

    next();
};

//endregion
