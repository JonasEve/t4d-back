/**
 * Created by JEVENISSE on 16/01/2017.
 */

exports.sendError = function (res, err) {

    res.status(err.status);
    res.send(err.message);
};

exports.handleError = function (res, err) {

    res.status(500);
    res.send(err.message);
};