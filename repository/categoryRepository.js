/**
 * Created by JEVENISSE on 16/01/2017.
 */

"use strict";

function getChildren(items) {

    return new Promise((resolve, reject) => {

        let promises = [];

        items.forEach(item => {
            promises.push(new Promise((resolve, reject) => {
                item.getChildren((err, children) => {

                    if (err) {
                        console.log(err);
                        return reject(err);
                    }

                    getChildren(children).then(() => {
                        return resolve();
                    }).catch(err => {
                        return reject(err);
                    });
                })
            }));
        });

        Promise.all(promises).then(function () {
            return resolve(items);
        }).catch(err => {
            return reject(err);
        });
    });
}

exports.getCategories = function (db) {

    return new Promise((resolve, reject) => {

        db.find({parent_id: null}, (err, items) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            getChildren(items).then(() => {
                    return resolve(items);
                }
            );
        });
    });
};

exports.createCategory = function (db, category) {

    if (!category.parent)
        category.parent = null;


    return new Promise((resolve, reject) => {
        db.create(
            {
                label: category.label,
                parent_id: category.parent,
            }, (err, item) => {

                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(item);
            });
    });
};

exports.updateCategory = function (db, id, category) {

    return new Promise((resolve, reject) => {

        db.one({id: id, active: true}, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            item.label = category.label;

            if (category.parent)
                item.parent_id = category.parent;

            item.save(err => {
                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(item);
            });
        });
    });
};

exports.deleteCategory = function (db, id) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            item.getChildren((err, children) => {

                let promises = [];

                children.forEach(child => {

                    child.parent_id = item.parent_id;
                    promises.push(new Promise((resolve, reject) =>
                        child.save(err => {
                            if (err) {
                                console.log(err);
                                return reject(err);
                            }

                            return resolve();
                        })))
                });

                Promise.all(promises).then(function () {
                    item.remove(err => {
                        if (err) {
                            console.log(err);
                            return reject(err);
                        }

                        return resolve();
                    });
                }).catch(err => {
                    return reject(err);
                });

            });
        });
    });
};