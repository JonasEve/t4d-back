/**
 * Created by JEVENISSE on 11/01/2017.
 */

"use strict";

exports.getQuizs = function (db) {

    return new Promise((resolve, reject) => {

        db.find({}, (err, items) => {

                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(items);
        });
    });
};

exports.getQuiz = function (db, id) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            return resolve(item);
        });
    });
};

exports.createQuiz = function (db, quiz) {

    return new Promise((resolve, reject) => {
        db.create(
            {
                name: quiz.name,
            }, (err, item) => {

                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(item);
            });
    });
};

exports.updateQuiz = function (db, id, quiz) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            item.name = quiz.name;

            item.save(err2 => {
                if (err2) {
                    console.log(err2);
                    return reject(err2);
                }

                return resolve(item);
            });
        });
    });
};

exports.deleteQuiz = function (db, id) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            console.log(err);
            if (err)
                return next(err);

            item.removeQuestion({}, err =>{

                if (err) {
                    console.log(err);
                    return reject(err);
                }

                item.remove(function (err2) {
                    if (err2) {
                        console.log(err2);
                        return reject(err2);
                    }

                    return resolve();
                });
            });

        });
    });
};


exports.addQuestion = function (db, id, question) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            console.log(err);
            if (err)
                return next(err);

            item.addQuestion(question, {position: 0, weight: 1}, err2 => {
                if (err2) {
                    console.log(err2);
                    return reject(err2);
                }

                return resolve();
            });
        });
    });
};

exports.deleteQuestion = function (db, id, question_id) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            console.log(err);
            if (err)
                return next(err);

            item.removeQuestion(question_id, err2 => {
                if (err2) {
                    console.log(err2);
                    return reject(err2);
                }

                return resolve();
            });
        });
    });
};