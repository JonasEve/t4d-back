/**
 * Created by JEVENISSE on 17/01/2017.
 */


exports.getAnswersByQuestionId = function (db, id) {

    return new Promise((resolve, reject) => {

        db.find({question_id: id}, (err, items) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            return resolve(items);
        });
    });
};

exports.updateAnswer = function (db, id, answer) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            item.correct = answer.correct;
            item.label = answer.label;

            item.save(err => {
                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(item);
            })
        });
    });
};

exports.deteleteAnswer = function (db, id) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {
            if (err) {
                console.log(err);
                return reject(err);
            }

            item.remove(err => {

                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve();
            });
        });
    });
};