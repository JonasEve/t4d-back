/**
 * Created by JEVENISSE on 11/01/2017.
 */

"use strict";

exports.getQuestion = function (db, id) {

    return new Promise((resolve, reject) => {
        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            return resolve(item);
        });
    });
};

exports.getQuestions = function (db) {

    return new Promise((resolve, reject) => {
        db.find({}, (err, items) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            return resolve(items);
        });
    });
};

exports.createQuestion = function (db, question) {

    if(!question.categories)
        question.categories = [];

    if(!question.answers)
        question.answers = [];

    return new Promise((resolve, reject) => {
        db.create(
            {
                label: question.label,
                type: question.type,
                answers: question.answers,
                category: question.categories,
            }, (err, item) => {

                if (err) {
                    console.log(err);
                    return reject(err);
                }


                return resolve(item);
            });
    });
};

exports.updateQuestion = function (db, id, question) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            console.log(err);
            if (err)
                return next(err);

            item.label = question.label;
            item.type = question.type;

            if(question.categories)
                item.answers = question.answers;

            if(question.answers)
                item.category = question.categories;

            item.save(function (err) {
                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(item);
            });
        });
    });
};

exports.deleteQuestion = function (db, id) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            console.log(err);
            if (err)
                return next(err);

            item.remove(err => {
                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve();
            });
        });
    });
};
