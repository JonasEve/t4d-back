/**
 * Created by JEVENISSE on 14/12/2016.
 */

exports.createUser = function (db, user) {

    return new Promise((resolve, reject) => {
        db.create(
            {
                firstname: user.firstname,
                surname: user.surname,
                email: user.email,
                password: user.password,
                role: user.role,
            }, (err, item) => {

                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(item);
            });
    });
};

exports.getUser = function (db, id) {

    return new Promise((resolve, reject) => {
        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);

            }

            item.getResults((err2, results) => {
                if (err2) {
                    console.log(err2);
                    return reject(err2);

                }

                item.results = results;

                return resolve(item);
            });
        })
    });
};

exports.getUsers = function (db) {

    return new Promise((resolve, reject) => {
        db.find({}, (err, items) => {

            if (err) {
                console.log(err);
                return reject(err);

            }

            return resolve(items);
        })
    });
};

exports.deleteUser = function (db, id) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return next(err);
            }


            item.remove(function (err3) {
                if (err3) {
                    console.log(err3);
                    return reject(err3);
                }

                return resolve();
            });
        });
    });
};

exports.updateUser = function (db, id, user) {

    return new Promise((resolve, reject) => {
        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            if (user.firstname)
                item.firstname = user.firstname;
            if (user.surname)
                item.surname = user.surname;
            if (user.email)
                item.email = user.email;
            if (user.role)
                item.role = user.role;
            if (user.password)
                item.password = user.password;

            item.save(err => {
                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(item);
            });
        })
    });
};

exports.findUser = function (db, value) {

    return new Promise((resolve, reject) => {
        db.one({email: value}, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);

            }

            return resolve(item);
        })
    });
};