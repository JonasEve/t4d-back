/**
 * Created by JEVENISSE on 16/01/2017.
 */

"use strict";

exports.createQuizResult = function (db, user_id, quiz) {

    return new Promise((resolve, reject) => {

        db.create(
            {
                data: JSON.stringify(quiz),
                user_id: user_id,
                date_creation: new Date(),
                status: 0
            }, (err, item) => {

                if (err) {
                    console.log(err);
                    return reject(err);
                }

                return resolve(item);
            });
    });
};

exports.deleteQuizResult = function (db, id) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            item.remove(err2 => {
                if (err2) {
                    console.log(err2);
                    return reject(err2);
                }

                return resolve();
            });

        });
    });
};

exports.deleteQuizResultByUserId = function (db, id) {

    return new Promise((resolve, reject) => {

        db.find({user_id: id}, (err, items) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            let promises = [];

            items.forEach(item => {
                promises.push(new Promise((resolve, reject) => {
                    item.remove(err2 => {
                        if (err2) {
                            console.log(err2);
                            return reject(err2);
                        }

                        return resolve();
                    });
                }));
            });

            Promise.all(promises)
                .then( () => {
                    return resolve();
                })
                .catch(err => {
                    return reject(err);
                })
        });
    });
};

exports.getQuizResultByUser = function (db, id) {

    return new Promise((resolve, reject) => {

        db.find({user_id: id}, (err, items) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            return resolve(items);
        });
    });
};

exports.updateQuizResult = function (db, id, result) {

    return new Promise((resolve, reject) => {

        db.get(id, (err, item) => {

            if (err) {
                console.log(err);
                return reject(err);
            }

            if(result.status > item.status)
                item.status = result.status;

            let data = JSON.parse(item.data);

            if(result.score || result.score === 0)
                data.score = result.score;

            for(let i = 0; i < data.question.length; i++){

                if(result.questions[i].correct || result.questions[i].correct === false)
                    data.question[i].correct = result.correct;

                for(let j = 0; j < data.question[i].answers.length; j++){
                    data.question[i].answers[j].userValue = result.questions[i].answers[j].userValue;
                }
            }

            item.data = JSON.stringify(data);

            item.save(err2 => {
                if (err2) {
                    console.log(err2);
                    return reject(err2);
                }

                return resolve(item);
            });
        });
    });
};